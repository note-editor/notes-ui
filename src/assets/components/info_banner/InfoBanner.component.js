import React from 'react';
import PropTypes from 'prop-types';

import {
  faTimesCircle,
} from '@fortawesome/free-solid-svg-icons';

import ButtonIcon from '../button_icon/ButtonIcon.component';

const InfoBanner = (props) => {
  const {
    display,
    message,
    type,
    close,
  } = props;

  if (display === false) {
    return null;
  }

  return (
    <div id="info-banner" className={type}>
      {message}
      <ButtonIcon
        funct={close}
        icon={faTimesCircle}
      />
    </div>
  );
};

InfoBanner.propTypes = {
  display: PropTypes.bool.isRequired,
  message: PropTypes.object.isRequired,
  type: PropTypes.string.isRequired,
  close: PropTypes.func.isRequired,
};

export default InfoBanner;
