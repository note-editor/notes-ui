import { connect } from 'react-redux';
import { resendLink } from '../../../../../redux/action_creators/Ui.actioncreator';
import NewVerLink from './NewVerLink.component';

const mapStateToProps = state => ({
  userData: state.Auth.userData,
});

const mapDispatchToProps = dispatch => ({
  resendLink: userData => dispatch(resendLink(userData)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(NewVerLink);
