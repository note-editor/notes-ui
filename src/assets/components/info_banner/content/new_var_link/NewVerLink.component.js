import React, { Fragment } from 'react';
import PropTypes from 'prop-types';

const NewVerLink = ({ userData, resendLink }) => (
  <Fragment>
    Warning account unverified, please check your emails for the verification link.&nbsp;
    Or click here to&nbsp;
    <span
      className="span-link"
      onMouseDown={() => {
        resendLink(userData);
      }}
      role="button"
      tabIndex={0}
    >
      resend link
    </span>
  </Fragment>
);

NewVerLink.propTypes = {
  userData: PropTypes.object.isRequired,
  resendLink: PropTypes.func.isRequired,
};

export default NewVerLink;
