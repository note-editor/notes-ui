import React from 'react';
import PropTypes from 'prop-types';

import InfoBanner from '../info_banner/InfoBanner.connector';

const Layout = ({ children }) => (
  <div>
    <InfoBanner />
    {children}
  </div>
);

Layout.propTypes = {
  children: PropTypes.object.isRequired,
};

export default Layout;
