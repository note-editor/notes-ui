import { connect } from 'react-redux';
import Layout from './Layout.component';

import { setInfoBanner } from '../../../redux/action_creators/Ui.actioncreator';

const mapStateToProps = state => ({
  token: state.Auth.bearerToken,
});

const mapDispatchToProps = dispatch => ({
  setInfoBanner: (display, type, message) => dispatch(setInfoBanner(display, type, message)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Layout);
