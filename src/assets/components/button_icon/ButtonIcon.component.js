import React from 'react';
import PropTypes from 'prop-types';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const ButtonIcon = ({ funct, icon, args }) => (
  <div
    className="button-icon"
    onMouseDown={
      () => {
        funct(...args);
      }
    }
    role="button"
    tabIndex={0}
  >
    <FontAwesomeIcon icon={icon} />
  </div>
);

ButtonIcon.defaultProps = {
  args: [],
};

ButtonIcon.propTypes = {
  icon: PropTypes.object.isRequired,
  funct: PropTypes.func.isRequired,
  args: PropTypes.array,
};

export default ButtonIcon;
