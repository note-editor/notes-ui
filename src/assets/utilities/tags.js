export function convertTagsToText(tags) {
  let tagsText = '';
  let tag;

  for (let i = 0; i < tags.length; i++) {
    tag = tags[i];

    if (typeof tag === 'string') {
      tagsText += tags[i];

      if (i !== tags.length - 1) {
        tagsText += ', ';
      }
    }
  }

  return tagsText;
}

export function convertTagsToObject(tagsdata) {
  const tags = [];
  tagsdata.forEach((tag) => {
    tags.push({
      id: tag,
      text: tag,
    });
  });
  return tags;
}
