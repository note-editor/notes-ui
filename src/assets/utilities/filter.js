export default function filter(item, criteria) {
  let match = false;
  let tag;

  if (item.title.search(criteria) !== -1) {
    match = true;
  }

  for (let i = 0; i < item.tags.length; i++) {
    tag = item.tags[i];

    if (typeof tag === 'string') {
      if (tag.search(criteria) !== -1) {
        match = true;
      }
    }
  }

  return match;
}
