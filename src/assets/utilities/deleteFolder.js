const recursiveSearch = (allFolders, toRemove) => {
  let foldersToRemove = [];
  let tmpList;
  const openList = allFolders.slice();
  let tmpID = '';

  while (openList.length !== 0) {
    if (openList[0].parent === toRemove) {
      tmpID = openList[0].id;
      openList.shift();
      tmpList = recursiveSearch(allFolders, tmpID);
      foldersToRemove = [...foldersToRemove, ...tmpList];
      foldersToRemove.push(tmpID);
    } else {
      openList.shift();
    }
  }

  return foldersToRemove;
};

export const findFoldersToRemove = (folders, id) => {
  const openList = [];
  let foldersToRemove = [];

  Object.keys(folders).map(e => openList.push(folders[e]));

  foldersToRemove = recursiveSearch(openList, id);
  foldersToRemove.push(id);

  return foldersToRemove;
};

export const findNotesToRemove = (notes, id) => {
  const keys = Object.keys(notes);
  const notesToRemove = [];

  keys.forEach((e) => {
    if (notes[e].parent === id) {
      notesToRemove.push(notes[e].id);
    }
  });

  return notesToRemove;
};
