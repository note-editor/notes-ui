import { connect } from 'react-redux';
import App from './App.component';
import { loadAuthData } from './redux/action_creators/Auth.actioncreator';


const mapStateToProps = state => ({
  offlineMode: state.Auth.offlineMode,
});

const mapDispatchToProps = dispatch => ({
  load: token => dispatch(loadAuthData(token)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(App);
