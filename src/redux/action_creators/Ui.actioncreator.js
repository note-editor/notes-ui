import React, { Fragment } from 'react';
import UiActions from '../operations/Ui.operations';

import {
  SET_FILTER_TEXT,
  SET_DRAGGED_ITEM,
  SET_FOLDER_EDIT,
  SET_MENU_DISPLAY,
  SET_INFO_BANNER,
} from '../Types';

export const setFilterText = filterText => ({
  type: SET_FILTER_TEXT,
  data: filterText,
});

export const setDraggedItem = (itemID, itemType) => ({
  type: SET_DRAGGED_ITEM,
  data: {
    id: itemID,
    type: itemType,
  },
});

export const setFolderEdit = (folderId, display) => ({
  type: SET_FOLDER_EDIT,
  data: {
    id: folderId,
    display: display,
  },
});

export const setMenuDisplay = display => ({
  type: SET_MENU_DISPLAY,
  data: {
    display: display,
  },
});

export const setInfoBanner = (display, type, message) => ({
  type: SET_INFO_BANNER,
  data: {
    display: display,
    type: type,
    message: message,
  },
});

export const resendLink = userData => async (dispatch) => {
  const payload = {
    userId: userData.id,
    email: userData.email,
    userName: userData.username,
    returnAdd: window.location.href,
  };

  UiActions.resendLink(payload);

  const message = (
    <Fragment>
      Email link sucessfully resent.
    </Fragment>
  );

  dispatch({
    type: SET_INFO_BANNER,
    data: {
      display: true,
      type: 'info',
      message: message,
    },
  });
};
