import store from '../../store';

import Delete from '../operations/Delete.operations';

import {
  TOGGLE_DELETE_MESSAGE,
  SET_NOTE_TO_DELETE,
  SET_FOLDER_TO_DELETE,
  DELETE_NOTE,
  DELETE_FOLDER,
} from '../Types';

import { findFoldersToRemove, findNotesToRemove } from '../../assets/utilities/deleteFolder';

export const toggleDeleteMessage = () => ({
  type: TOGGLE_DELETE_MESSAGE,
});

export const setNoteToDelete = noteId => (dispatch) => {
  dispatch([
    { type: SET_NOTE_TO_DELETE, data: noteId },
    { type: TOGGLE_DELETE_MESSAGE },
  ]);
};

export const setFolderToDelete = folderId => (dispatch) => {
  dispatch([
    { type: SET_FOLDER_TO_DELETE, data: folderId },
    { type: TOGGLE_DELETE_MESSAGE },
  ]);
};

export const deleteNote = noteId => async (dispatch) => {
  const result = await Delete.note(noteId);

  if (result) {
    return (
      dispatch([
        { type: TOGGLE_DELETE_MESSAGE },
        { type: DELETE_NOTE, data: noteId },
        { type: SET_NOTE_TO_DELETE, data: '' },
      ])
    );
  }

  return (
    dispatch([
      { type: TOGGLE_DELETE_MESSAGE },
      { type: DELETE_NOTE, data: noteId },
    ])
  );
};

export const deleteFolder = folderId => async (dispatch) => {
  const foldersToRemove = findFoldersToRemove(store.getState().Folders.data, folderId);
  let notesToRemove = [];

  foldersToRemove.forEach((folder) => {
    Delete.folder(folder);
    notesToRemove = findNotesToRemove(store.getState().Notes.data, folder);
    notesToRemove.forEach((note) => {
      Delete.note(note);
    });
  });

  return (
    dispatch([
      { type: TOGGLE_DELETE_MESSAGE },
      { type: DELETE_FOLDER, data: foldersToRemove, top: folderId },
      { type: SET_FOLDER_TO_DELETE, data: '' },
    ])
  );
};
