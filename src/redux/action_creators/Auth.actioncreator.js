import React from 'react';
import Auth from '../operations/Auth.operations';
import store from '../../store';
import { SET_BEARER_TOKEN, SET_USER_DATA, SET_INFO_BANNER } from '../Types';
import { deleteCookie } from '../../assets/utilities/cookies';
import NewVerLink from '../../assets/components/info_banner/content/new_var_link/NewVerLink.connector';

import { loadNotes } from './Notes.actioncreator';
import { loadFolders } from './Folders.actioncreator';

export const getUserData = () => async (dispatch) => {
  const result = await Auth.getUserData();
  const { userInfo } = result.data;

  // Set user data
  await dispatch({
    type: SET_USER_DATA,
    data: userInfo,
  });

  // If user account is unverified display banner
  if (!userInfo.verified) {
    dispatch({
      type: SET_INFO_BANNER,
      data: {
        display: true,
        type: 'error',
        message: <NewVerLink />,
      },
    });
  }
};

export const loadAuthData = token => async (dispatch) => {
  // Set user data
  await dispatch({
    type: SET_BEARER_TOKEN,
    data: token,
  });

  store.dispatch(getUserData());
  store.dispatch(loadNotes());
  store.dispatch(loadFolders());
};

// Clears the bearer token.
export const clearBearerToken = () => (dispatch) => {
  deleteCookie('authData');

  dispatch([
    {
      type: 'RESET_STATE',
    },
    {
      type: SET_BEARER_TOKEN,
      data: '',
    },
  ]);

  window.location.href = `${process.env.REACT_APP_AUTH_URL}?returnAdd=${window.location.href}`;
};
