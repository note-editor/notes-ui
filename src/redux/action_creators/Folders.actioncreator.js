import store from '../../store';
import FoldersActions from '../operations/Folders.operations';
import {
  LOAD_FOLDERS,
  ADD_FOLDER,
  SET_CURRENT_FOLDER,
  UPDATE_FOLDER,
  SET_FOLDER_EDIT,
} from '../Types';

export const loadFolders = () => async (dispatch) => {
  const result = await FoldersActions.loadFolders();

  return (
    dispatch({
      type: LOAD_FOLDERS,
      data: result,
    })
  );
};

export const addFolder = parentId => async (dispatch) => {
  const result = await FoldersActions.newFolder(parentId);

  const folder = {
    id: result._id,
    title: result.title,
    tags: result.tags,
    parent: result.parent,
  };

  return (
    dispatch([
      {
        type: ADD_FOLDER,
        data: folder,
      },
      {
        type: SET_FOLDER_EDIT,
        data: {
          id: result._id,
          display: true,
        },
      },
    ])
  );
};

export const setCurrentFolder = folderId => ({
  type: SET_CURRENT_FOLDER,
  data: folderId,
});

export const updateFolder = (folderId, key, value) => (dispatch) => {
  const data = {
    value: value,
    key: key,
    folder: folderId,
  };

  return (
    dispatch({
      type: UPDATE_FOLDER,
      data: data,
    })
  );
};

export const saveFolder = folderId => (dispatch) => {
  const data = store.getState().Folders.data[folderId];

  if (!store.getState().Auth.offlineMode) {
    FoldersActions.updateFolder(folderId, data);
  }

  return (
    dispatch({ type: '' })
  );
};
