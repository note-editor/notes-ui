import {
  SET_TAGS,
  ADD_TAG,
} from '../Types';

export const setTags = tags => ({
  type: SET_TAGS,
  data: tags,
});

export const addTag = tag => ({
  type: ADD_TAG,
  data: tag,
});
