import store from '../../store';
import NotesActions from '../operations/Notes.operations';
import {
  LOAD_NOTES,
  ADD_NOTE,
  UPDATE_NOTE,
  SET_CURRENT_NOTE,
  SET_TO_SAVE,
} from '../Types';

/**
 * Loads the notes from the backend
 *
 * @param array notes
 */
export const loadNotes = () => async (dispatch) => {
  const result = await NotesActions.loadNotes();

  return (
    dispatch({
      type: LOAD_NOTES,
      data: result,
    })
  );
};

/**
 * Sets the current note.
 *
 * @param object currentNote
 */
export const setCurrentNote = noteId => dispatch => (
  dispatch({
    type: SET_CURRENT_NOTE,
    data: noteId,
  })
);

/**
 * Adds a new note to the notes meta array.
 */
export const addNote = parentId => async (dispatch) => {
  const result = await NotesActions.newNote(parentId);

  const note = {
    id: result._id,
    title: result.title,
    tags: ((result.tags !== '') ? result.tags.split(',') : []),
    parent: result.parent,
    content: result.content,
  };

  return (
    dispatch([
      { type: ADD_NOTE, data: note },
      { type: SET_CURRENT_NOTE, data: result._id },
    ])
  );
};

/**
 * Updates an instance of the note meta
 *
 * @param object note
 */
export const updateNote = (noteId, key, value) => (dispatch) => {
  const data = {
    value: value,
    key: key,
    note: noteId,
  };

  return (
    dispatch({
      type: UPDATE_NOTE,
      data: data,
    })
  );
};

/**
 * Saves a note in the backend and marks that a note has been saved
 *
 * @param string noteId
 */
export const saveNote = noteId => (dispatch) => {
  const data = store.getState().Notes.data[noteId];

  if (!store.getState().Auth.offlineMode) {
    NotesActions.updateNote(noteId, data);
  }

  return (
    dispatch({
      type: SET_TO_SAVE,
      data: false,
    })
  );
};
