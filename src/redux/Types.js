// Auth
export const SET_BEARER_TOKEN = 'SET_BEARER_TOKEN';
export const SET_USER_DATA = 'SET_USER_DATA';

// Delete
export const TOGGLE_DELETE_MESSAGE = 'TOGGLE_DELETE_MESSAGE';
export const SET_NOTE_TO_DELETE = 'SET_NOTE_TO_DELETE';
export const SET_FOLDER_TO_DELETE = 'SET_FOLDER_TO_DELETE';

// Folders
export const LOAD_FOLDERS = 'LOAD_FOLDERS';
export const ADD_FOLDER = 'ADD_FOLDER';
export const DELETE_FOLDER = 'DELETE_FOLDER';
export const SET_CURRENT_FOLDER = 'SET_CURRENT_FOLDER';
export const UPDATE_FOLDER = 'UPDATE_FOLDER';
export const RESET_FOLDERS = 'RESET_FOLDERS';

// Notes
export const UPDATE_NOTE = 'UPDATE_NOTE';
export const ADD_NOTE = 'ADD_NOTE';
export const SET_CURRENT_NOTE = 'SET_CURRENT_NOTE';
export const LOAD_NOTES = 'LOAD_NOTES';
export const DELETE_NOTE = 'DELETE_NOTE';
export const SET_TO_SAVE = 'SET_TO_SAVE';
export const RESET_NOTES = 'RESET_NOTES';

// Tags
export const SET_TAGS = 'SET_TAGS';
export const ADD_TAG = 'ADD_TAG';
export const RESET_TAGS = 'RESET_TAGS';

// UI
export const SET_FILTER_TEXT = 'SET_FILTER_TEXT';
export const SET_DRAGGED_ITEM = 'SET_DRAGGED_ITEM';
export const SET_FOLDER_EDIT = 'SET_FOLDER_EDIT';
export const SET_MENU_DISPLAY = 'SET_MENU_DISPLAY';
export const SET_INFO_BANNER = 'SET_INFO_BANNER';
export const RESET_UI = 'RESET_UI';
