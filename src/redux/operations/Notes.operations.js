import api from '../../assets/utilities/api';
import store from '../../store';
import { setTags } from '../action_creators/Tags.actioncreator';

export default {
  /**
   * Creates a new note in the backend and returns the newly created note.
   *
   * @returns {object}
   */
  async newNote(parent) {
    let result;

    // App is in offline mode.
    if (store.getState().Auth.offlineMode) {
      result = {
        content: '',
        _id: `offline-${Math.floor(Math.random() * 9999) + 1}`,
        title: 'New Note',
        tags: '',
        parent: parent,
      };

      return result;
    }

    const payload = {
      parent: parent,
    };

    result = await api.post(`${process.env.REACT_APP_API_URL}/notes`, payload, store.getState().Auth.bearerToken);
    return result.data;
  },

  async loadNotes() {
    const result = await api.get(`${process.env.REACT_APP_API_URL}/notes`, store.getState().Auth.bearerToken);

    if (!('data' in result)) {
      return {};
    }

    const newNotes = {};
    let note = {};
    let noteTags = [];
    const allTags = [];

    result.data.forEach((notedata) => {
      noteTags = ((notedata.tags !== '') ? notedata.tags.split(',') : []);

      note = {
        content: notedata.content,
        id: notedata._id,
        title: notedata.title,
        tags: noteTags,
        parent: notedata.parent,
      };

      note.tags.forEach((tag) => {
        if (!allTags.includes(tag)) {
          allTags.push(tag);
        }
      });

      newNotes[note.id] = note;
    });

    store.dispatch(setTags(allTags));

    return newNotes;
  },

  updateNote(noteId, data) {
    api.patch(`${process.env.REACT_APP_API_URL}/notes/${noteId}`, data, store.getState().Auth.bearerToken);
  },
};
