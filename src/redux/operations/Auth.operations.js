import api from '../../assets/utilities/api';
import store from '../../store';

export default {
  /**
   * Returns the user data.
   *
   * @returns {object}
   */
  async getUserData() {
    const result = await api.get(`${process.env.REACT_APP_AUTH_API_URL}/userdata`, store.getState().Auth.bearerToken);
    return result;
  },
};
