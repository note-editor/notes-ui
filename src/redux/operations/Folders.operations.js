import api from '../../assets/utilities/api';
import store from '../../store';

export default {
  async newFolder(parent) {
    let result;

    // App is in offline mode.
    if (store.getState().Auth.offlineMode) {
      result = {
        _id: `offline-${Math.floor(Math.random() * 9999) + 1}`,
        title: 'New folder',
        tags: '',
        parent: parent,
      };

      return result;
    }
    const payload = {
      parent: parent,
    };

    result = await api.post(`${process.env.REACT_APP_API_URL}/folders`, payload, store.getState().Auth.bearerToken);
    return result.data;
  },

  async loadFolders() {
    const result = await api.get(`${process.env.REACT_APP_API_URL}/folders`, store.getState().Auth.bearerToken);

    if (!('data' in result)) {
      return {};
    }

    const newFolders = {};
    let folder = {};
    let tmpTags = [];

    result.data.forEach((folderdata) => {
      tmpTags = ((folderdata.tags !== '') ? folderdata.tags.split(',') : []);
      folder = {
        id: folderdata._id,
        title: folderdata.title,
        tags: tmpTags,
        parent: folderdata.parent,
      };

      newFolders[folder.id] = folder;
    });

    return newFolders;
  },

  updateFolder(folderId, data) {
    api.patch(`${process.env.REACT_APP_API_URL}/folders/${folderId}`, data, store.getState().Auth.bearerToken);
  },
};
