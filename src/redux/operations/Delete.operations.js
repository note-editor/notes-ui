import api from '../../assets/utilities/api';
import store from '../../store';

export default {
  /**
   * Deletes the specified note
   *
   * @returns {object}
   */
  async note(noteId) {
    const result = await api.delete(`${process.env.REACT_APP_API_URL}/notes/${noteId}`, store.getState().Auth.bearerToken);
    return result;
  },

  /**
   * Deletes the specified folder
   *
   * @returns {object}
   */
  async folder(folderId) {
    const result = await api.delete(`${process.env.REACT_APP_API_URL}/folders/${folderId}`, store.getState().Auth.bearerToken);
    return result;
  },
};
