import {
  LOAD_NOTES,
  ADD_NOTE,
  UPDATE_NOTE,
  DELETE_NOTE,
  SET_CURRENT_NOTE,
  SET_TO_SAVE,
} from '../Types';

const emptyNote = {
  id: '0',
  title: '',
  content: '',
  tags: [],
  parent: 'blank',
};

const initialState = {
  data: {
    0: emptyNote,
  },
  currentNote: null,
  toSave: false,
};

const Notes = (state = initialState, action) => {
  switch (action.type) {
    case LOAD_NOTES:
      action.data[0] = emptyNote;
      return {
        ...state,
        data: action.data,
      };
    case ADD_NOTE:
      return {
        ...state,
        data: {
          ...state.data,
          [action.data.id]: action.data,
        },
      };
    case UPDATE_NOTE:
      return {
        ...state,
        data: {
          ...state.data,
          [action.data.note]: {
            ...state.data[action.data.note],
            [action.data.key]: action.data.value,
          },
        },
        toSave: true,
      };
    case DELETE_NOTE: {
      if (action.data === state.currentNote) {
        state = {
          ...state,
          currentNote: '0',
        };
      }

      const { data: notes, ...noChild } = state;
      const { [action.data]: removedValue, ...notesWithout } = notes;
      const without = { ...noChild, data: notesWithout };

      return without;
    }
    case SET_CURRENT_NOTE:
      return {
        ...state,
        currentNote: action.data,
      };
    case SET_TO_SAVE:
      return {
        ...state,
        toSave: action.data,
      };
    case 'RESET_STATE':
      return initialState;
    default:
      return state;
  }
};

export default Notes;
