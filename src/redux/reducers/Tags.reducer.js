import {
  SET_TAGS,
  ADD_TAG,
} from '../Types';

const initialState = {
  data: [],
};

const Tags = (state = initialState, action) => {
  switch (action.type) {
    case SET_TAGS:
      return {
        ...state,
        data: action.data,
      };
    case ADD_TAG:
      if (!state.data.includes(action.tag)) {
        return {
          ...state,
          data: [
            ...state.data,
            action.data,
          ],
        };
      }
      return state;
    case 'RESET_STATE':
      return initialState;
    default:
      return state;
  }
};

export default Tags;
