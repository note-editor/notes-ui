import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';

import Notes from './Notes.reducer';
import Folders from './Folders.reducer';
import Delete from './Delete.reducer';
import Tags from './Tags.reducer';
import Ui from './Ui.reducer';
import Auth from './Auth.reducer';

const rootReducer = history => combineReducers({
  Notes,
  Folders,
  Delete,
  Tags,
  Ui,
  Auth,
  router: connectRouter(history),
});

export default rootReducer;
