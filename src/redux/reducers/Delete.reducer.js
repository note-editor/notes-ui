import {
  TOGGLE_DELETE_MESSAGE,
  SET_NOTE_TO_DELETE,
  SET_FOLDER_TO_DELETE,
} from '../Types';

const initialState = {
  display: false,
  noteId: '',
  folderId: '',
};

const NoteToDelete = (state = initialState, action) => {
  switch (action.type) {
    case TOGGLE_DELETE_MESSAGE: {
      const newDis = !state.display;

      if (newDis === false) {
        return {
          display: newDis,
          noteId: '',
          folderId: '',
        };
      }

      return {
        ...state,
        display: newDis,
      };
    }
    case SET_NOTE_TO_DELETE:
      return {
        ...state,
        noteId: action.data,
      };
    case SET_FOLDER_TO_DELETE:
      return {
        ...state,
        folderId: action.data,
      };
    case 'RESET_STATE':
      return initialState;
    default:
      return state;
  }
};

export default NoteToDelete;
