import {
  LOAD_FOLDERS,
  ADD_FOLDER,
  UPDATE_FOLDER,
  DELETE_FOLDER,
  SET_CURRENT_FOLDER,
} from '../Types';

function removeFolder(list, id) {
  const { [id]: removedValue, ...foldersWithout } = list;
  return foldersWithout;
}

const initialState = {
  data: {},
  currentFolder: null,
};

const Folders = (state = initialState, action) => {
  switch (action.type) {
    case LOAD_FOLDERS:
      return {
        ...state,
        data: action.data,
      };
    case ADD_FOLDER:
      return {
        ...state,
        data: {
          ...state.data,
          [action.data.id]: action.data,
        },
      };
    case UPDATE_FOLDER:
      return {
        ...state,
        data: {
          ...state.data,
          [action.data.folder]: {
            ...state.data[action.data.folder],
            [action.data.key]: action.data.value,
          },
        },
      };
    case DELETE_FOLDER: {
      if (action.top === state.currentFolder) {
        state = {
          ...state,
          currentFolder: null,
        };
      }

      const { data: folders, ...otherData } = state;
      let foldersWithout = folders;

      action.data.forEach((folder) => {
        foldersWithout = removeFolder(foldersWithout, folder);
      });

      const without = { ...otherData, data: foldersWithout };

      return without;
    }
    case SET_CURRENT_FOLDER:
      return {
        ...state,
        currentFolder: action.data,
      };
    case 'RESET_STATE':
      return initialState;
    default:
      return state;
  }
};

export default Folders;
