import {
  SET_FILTER_TEXT,
  SET_DRAGGED_ITEM,
  SET_FOLDER_EDIT,
  SET_MENU_DISPLAY,
  SET_INFO_BANNER,
} from '../Types';

const initialState = {
  draggedItem: {
    type: null,
    id: null,
  },
  folderEdit: {
    display: false,
    id: '',
  },
  menuDisplay: false,
  filterText: '',
  infoBanner: {
    display: false,
    message: {},
    type: '',
  },
};

const Ui = (state = initialState, action) => {
  switch (action.type) {
    case SET_FILTER_TEXT:
      return {
        ...state,
        filterText: action.data,
      };
    case SET_DRAGGED_ITEM:
      return {
        ...state,
        draggedItem: {
          id: action.data.id,
          type: action.data.type,
        },
      };
    case SET_FOLDER_EDIT:
      return {
        ...state,
        folderEdit: {
          id: action.data.id,
          display: action.data.display,
        },
      };
    case SET_MENU_DISPLAY:
      return {
        ...state,
        menuDisplay: action.data.display,
      };
    case SET_INFO_BANNER:
      return {
        ...state,
        infoBanner: action.data,
      };
    case 'RESET_STATE':
      return initialState;
    default:
      return state;
  }
};

export default Ui;
