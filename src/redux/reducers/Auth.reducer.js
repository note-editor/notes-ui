import { getCookie } from '../../assets/utilities/cookies';
import {
  SET_BEARER_TOKEN,
  SET_USER_DATA,
} from '../Types';

const initialState = {
  bearerToken: getCookie('bearerToken') || null,
  userData: null,
  offlineMode: (process.env.REACT_APP_OFFLINE === 'true'),
};

const Auth = (state = initialState, action) => {
  switch (action.type) {
    case SET_BEARER_TOKEN:
      return {
        ...state,
        bearerToken: action.data,
      };
    case SET_USER_DATA:
      return {
        ...state,
        userData: action.data,
      };
    case 'RESET_STATE':
      return initialState;
    default:
      return state;
  }
};

export default Auth;
