import store from '../../store';
import { mockFetch } from './helpers/mock.helpers';

import {
  toggleDeleteMessage,
  setNoteToDelete,
  setFolderToDelete,
  deleteNote,
  deleteFolder,
} from '../../redux/action_creators/Delete.actioncreator';
import { loadNotes } from '../../redux/action_creators/Notes.actioncreator';
import { loadFolders } from '../../redux/action_creators/Folders.actioncreator';

import { deleteNotesData } from './data/delete.data';
import { loadNotesData } from './data/notes.data';
import { loadFoldersData } from './data/folders.data';

/**
 * Test toggle delete message
 *
 * Delete message display flag to be true
 */
test('should sucessfully toggle delete message', async () => {
  await store.dispatch(toggleDeleteMessage());
  expect(store.getState().Delete.display).toEqual(true);
  await store.dispatch(toggleDeleteMessage());
  expect(store.getState().Delete.display).toEqual(false);
});

/**
 * Test set note to delete
 *
 * Delete message display flag to be true
 * Delete note Id to be set correctly
 */
test('should sucessfully set note to delete', async () => {
  await store.dispatch(setNoteToDelete('1'));
  expect(store.getState().Delete.display).toEqual(true);
  expect(store.getState().Delete.noteId).toEqual('1');

  await store.dispatch(toggleDeleteMessage());
  expect(store.getState().Delete.noteId).toEqual('');
});

/**
 * Test set folder to delete
 *
 * Delete message display flag to be true
 * Delete note Id to be set correctly
 */
test('should sucessfully set folder to delete', async () => {
  await store.dispatch(setFolderToDelete('1'));
  expect(store.getState().Delete.display).toEqual(true);
  expect(store.getState().Delete.folderId).toEqual('1');

  await store.dispatch(toggleDeleteMessage());
  expect(store.getState().Delete.folderId).toEqual('');
});

/**
 * Test delete note
 *
 * Delete message display flag to be false
 * Note sucessfully deleted
 */
test('should delete note', async () => {
  // Set up for notes delete
  mockFetch(loadNotesData);
  await store.dispatch(loadNotes());
  await store.dispatch(setNoteToDelete('1'));
  expect(store.getState().Notes.data).toHaveProperty('1');

  mockFetch(deleteNotesData);
  await store.dispatch(deleteNote('1'));

  expect(store.getState().Delete.display).toEqual(false);
  expect(store.getState().Delete.noteId).toEqual('');
  expect(store.getState().Notes.data).not.toHaveProperty('1');
});

/**
 * Test delete folder
 *
 * Delete message display flag to be false
 * folder sucessfully deleted
 */
test('should delete folder', async () => {
  // Set up for folder delete
  // Load in folder
  mockFetch(loadFoldersData);
  await store.dispatch(loadFolders());
  await store.dispatch(setFolderToDelete('1'));
  expect(store.getState().Folders.data).toHaveProperty('1');

  mockFetch({});
  await store.dispatch(deleteFolder('1'));

  expect(store.getState().Delete.display).toEqual(false);
  expect(store.getState().Delete.folderId).toEqual('');
  expect(store.getState().Folders.data).not.toHaveProperty('1');
});
