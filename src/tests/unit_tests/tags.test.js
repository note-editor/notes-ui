import store from '../../store';

import {
  setTags,
  addTag,
} from '../../redux/action_creators/Tags.actioncreator';


/**
 * Tests set tags
 *
 * Expect tags to equal expected
 */
test('should set tags', async () => {
  await store.dispatch(setTags(['test1', 'test2']));
  expect(store.getState().Tags.data).toEqual(['test1', 'test2']);
});

/**
 * Tests add tags
 *
 * Expect tags to equal expected
 */
test('should add tags', async () => {
  await store.dispatch(addTag('test3'));
  expect(store.getState().Tags.data).toEqual(['test1', 'test2', 'test3']);
});
