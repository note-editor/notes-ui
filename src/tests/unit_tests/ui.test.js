import store from '../../store';

import {
  setFilterText,
  setDraggedItem,
  setFolderEdit,
  setMenuDisplay,
  setInfoBanner,
  resendLink,
} from '../../redux/action_creators/Ui.actioncreator';

/**
 * Tests set filter text
 *
 * Expect filter text to equal expected
 */
test('should set filter text', async () => {
  await store.dispatch(setFilterText('test1'));
  expect(store.getState().Ui.filterText).toEqual('test1');
});

/**
 * Tests set dragged item
 *
 * Expect dragged item to equal expected
 */
test('should set dragged item', async () => {
  await store.dispatch(setDraggedItem('id', 'type'));
  expect(store.getState().Ui.draggedItem).toEqual({ type: 'type', id: 'id' });
});

/**
 * Tests set folder edit
 *
 * Expect folder edit to equal expected
 */
test('should set folder edit', async () => {
  await store.dispatch(setFolderEdit('id', true));
  expect(store.getState().Ui.folderEdit).toEqual({ display: true, id: 'id' });
});

/**
 * Tests set folder edit
 *
 * Expect folder edit to equal expected
 */
test('should set menu display', async () => {
  await store.dispatch(setMenuDisplay(true));
  expect(store.getState().Ui.menuDisplay).toEqual(true);
});

/**
 * Set info banner
 *
 * Expect info banner to equal expected
 */
test('should set info banner', async () => {
  await store.dispatch(setInfoBanner(true, 'type', 'message'));
  expect(store.getState().Ui.infoBanner).toEqual({ display: true, type: 'type', message: 'message' });
});

/**
 * Set resend link
 *
 * Expect resend link to equal expected
 */
test('should set resend link', async () => {
  await store.dispatch(resendLink({ id: 'id', email: 'email', username: 'username' }));
  expect(store.getState().Ui.infoBanner.display).toEqual(true);
});
