import store from '../../store';

import {
  loadFolders,
  setCurrentFolder,
  addFolder,
  updateFolder,
} from '../../redux/action_creators/Folders.actioncreator';
import { mockFetch } from './helpers/mock.helpers';

import {
  loadFoldersData,
  addFoldersData,
} from './data/folders.data';

/**
 * Test Sucessful loading of folders
 *
 * Expect stored folder to equal expected folder
 * Expect current folder to equal 0
 */
test('should sucessfully load folders data', async () => {
  const expectedFolder = {
    id: '1',
    parent: null,
    tags: [],
    title: 'test',
  };

  mockFetch(loadFoldersData);
  await store.dispatch(loadFolders());

  expect(store.getState().Folders.data[1]).toMatchObject(expectedFolder);
  expect(store.getState().Folders.currentFolder).toEqual(null);
});

/**
 * Test setting the current folder
 *
 * Expect current folder to equal 1
 */
test('should sucessfully set the current folder', async () => {
  await store.dispatch(setCurrentFolder('1'));
  expect(store.getState().Folders.currentFolder).toEqual('1');
});

/**
 * Test adding a folder
 *
 * Expect stored folder to equal expected folder
 */
test('should sucessfully add a folder', async () => {
  const expectedFolder = {
    id: '2',
    parent: null,
    tags: '',
    title: 'New folder',
  };

  mockFetch(addFoldersData);
  await store.dispatch(addFolder(1));

  expect(store.getState().Folders.data[2]).toMatchObject(expectedFolder);
});

/**
 * Test updating a folder
 *
 * Expect notes content to sucessfully update
 * Expect toSave flag to be true
 */
test('should sucessfully update a note', async () => {
  await store.dispatch(updateFolder(1, 'title', 'test update'));
  expect(store.getState().Folders.data[1].title).toEqual('test update');

  await store.dispatch(updateFolder(1, 'parent', 'test update'));
  expect(store.getState().Folders.data[1].parent).toEqual('test update');
});
