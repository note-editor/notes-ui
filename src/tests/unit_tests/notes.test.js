import store from '../../store';

import {
  loadNotes,
  setCurrentNote,
  addNote,
  updateNote,
  saveNote,
} from '../../redux/action_creators/Notes.actioncreator';
import { mockFetch } from './helpers/mock.helpers';

import {
  loadNotesData,
  addNoteData,
} from './data/notes.data';

/**
 * Test Sucessful loading of notes
 *
 * Expect stored note to equal expected note
 * Expect current note to equal 0
 */
test('should sucessfully load notes data', async () => {
  const expectedNote = {
    content: '',
    id: '1',
    parent: null,
    tags: ['tag1', 'tag2'],
    title: 'New Note 1',
  };

  mockFetch(loadNotesData);
  await store.dispatch(loadNotes());

  expect(store.getState().Notes.data[1]).toMatchObject(expectedNote);
  expect(store.getState().Notes.currentNote).toEqual(null);
});

/**
 * Test setting the current note
 *
 * Expect current note to equal 1
 */
test('should sucessfully set the current note', async () => {
  await store.dispatch(setCurrentNote('1'));
  expect(store.getState().Notes.currentNote).toEqual('1');
});

/**
 * Test adding a note
 *
 * Expect current note to equal 2
 * Expect stored note to equal expected note
 */
test('should sucessfully add a note', async () => {
  const expectedNote = {
    content: '',
    id: '2',
    parent: null,
    tags: [],
    title: 'New Note 2',
  };

  mockFetch(addNoteData);
  await store.dispatch(addNote(1));

  expect(store.getState().Notes.currentNote).toEqual('2');
  expect(store.getState().Notes.data[2]).toMatchObject(expectedNote);
});

/**
 * Test updating a note
 *
 * Expect notes content to sucessfully update
 * Expect toSave flag to be true
 */
test('should sucessfully update a note', async () => {
  await store.dispatch(updateNote(1, 'content', 'test update'));
  expect(store.getState().Notes.data[1].content).toEqual('test update');

  await store.dispatch(updateNote(1, 'title', 'test update'));
  expect(store.getState().Notes.data[1].title).toEqual('test update');

  await store.dispatch(updateNote(1, 'parent', 'test update'));
  expect(store.getState().Notes.data[1].parent).toEqual('test update');
  expect(store.getState().Notes.toSave).toEqual(true);
});

/**
 * Test saving a note
 *
 * Expect toSave flag to be false
 */
test('should sucessfully save a note', async () => {
  await store.dispatch(saveNote('1'));
  expect(store.getState().Notes.toSave).toEqual(false);
});
