import store from '../../store';

import { clearBearerToken } from '../../redux/action_creators/Auth.actioncreator';

/**
 * Test logout
 *
 * Expect bearer token to be empty in store
 * Expect bearer token not to be set in local storage
 * Expect user data not to be set in store
 */
test('should sucessfully log user out', async () => {
  await store.dispatch(clearBearerToken());

  expect(store.getState().Auth.bearerToken).toEqual('');
  expect(localStorage.getItem('bearerToken')).toEqual(null);
  expect(store.getState().Auth.userData).toEqual(null);
});
