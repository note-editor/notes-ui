const sucessfulLoginData = {
  data: {
    token: 'testToken',
    userInfo: {
      email: 'test@test.com',
      id: 'testId',
      username: 'test',
      verified: true,
    },
  },
};

const failedLoginData = {
  errors: [{
    code: 'a-e-000003',
    title: 'Test Title',
    status: 400,
    detail: 'Test Message',
  }],
};

const unverifiedData = {
  data: {
    token: 'testToken',
    userInfo: {
      email: 'test@test.com',
      id: 'testId',
      username: 'test',
      verified: false,
    },
  },
};

const registerUser = {
  data: {
    token: 'testToken',
    userInfo: {
      email: 'test@test.com',
      id: 'testId',
      username: 'test',
      verified: false,
    },
  },
};

const failedRegistration = {
  errors: [{
    code: 'a-e-000004',
    title: 'Already exists',
    status: 400,
    detail: 'A user with this email already exists within our system.',
  }],
};

export {
  sucessfulLoginData,
  failedLoginData,
  unverifiedData,
  registerUser,
  failedRegistration,
};
