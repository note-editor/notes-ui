const deleteNotesData = {
  data: [
    {
      content: '',
      parent: null,
      tags: 'tag1,tag2',
      title: 'New Note 1',
      _id: '1',
      user: '5c661e101e0e2290451fc596',
      createdAt: '2019-02-15T02: 06: 45.089Z',
      updatedAt: '2019-02-15T02: 06: 45.089Z',
      __v: 0,
    },
  ],
};

export {
  deleteNotesData,
};
