const loadFoldersData = {
  data: [
    {
      parent: null,
      tags: '',
      title: 'test',
      _id: '1',
      user: '1',
      createdAt: '2019-02-15T02:29:18.350Z',
      updatedAt: '2019-02-15T02:29:18.350Z',
      __v: 0,
    },
  ],
};

const addFoldersData = {
  data: {
    parent: null,
    tags: '',
    title: 'New folder',
    _id: '2',
    user: '1',
    createdAt: '2019-03-22T10:29:41.985Z',
    updatedAt: '2019-03-22T10:29:41.985Z',
    __v: 0,
  },
};

export {
  loadFoldersData,
  addFoldersData,
};
