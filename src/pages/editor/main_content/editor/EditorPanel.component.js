import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import Tags from './editor_panel/tags/Tags.connector';
import TitleInput from './editor_panel/title_input/TitleInput.connector';
import NoteEditorContainer from './editor_panel/note_editor/NoteEditor.connector';

const EditorPanel = ({ currentNote }) => (
  currentNote ? (
    <div id="editor-wrapper">
      <div
        id="editor-top-bar"
        className="top-bar-height"
      >
        <div id="title-input-container">
          <TitleInput />
        </div>
        <div id="tags-input-container">
          <Tags
            key={currentNote}
          />
        </div>
      </div>
      <div id="editor-container">
        <NoteEditorContainer
          key={currentNote}
        />
      </div>
    </div>
  ) : null
);

EditorPanel.defaultProps = {
  currentNote: null,
};

EditorPanel.propTypes = {
  currentNote: PropTypes.string,
};

export default connect()(EditorPanel);
