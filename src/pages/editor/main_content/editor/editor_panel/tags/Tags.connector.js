import { connect } from 'react-redux';
import { updateNote, saveNote } from '../../../../../../redux/action_creators/Notes.actioncreator';
import { addTag } from '../../../../../../redux/action_creators/Tags.actioncreator';
import Tags from './Tags.component';

const mapStateToProps = state => ({
  currentNote: state.Notes.currentNote,
  noteTags: state.Notes.data[state.Notes.currentNote].tags,
  inTags: state.Tags.data,
});

const mapDispatchToProps = dispatch => ({
  updateNote: (currentNote, tags) => dispatch(updateNote(currentNote, 'tags', tags)),
  addTag: tag => dispatch(addTag(tag)),
  saveNote: currentNote => dispatch(saveNote(currentNote)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Tags);
