import React, { useState } from 'react';
import PropTypes from 'prop-types';
import TagsInput from 'react-tagsinput';
import 'react-tagsinput/react-tagsinput.css';

const Tags = (props) => {
  const { inTags } = props;
  const [tags, setTags] = useState(inTags);

  const handleChange = async (newTags) => {
    const { updateNote, currentNote, saveNote } = props;
    setTags(newTags);
    await updateNote(currentNote, newTags);
    saveNote(currentNote);
  };

  return (
    <TagsInput
      value={tags}
      onChange={(e) => {
        handleChange(e);
      }}
    />
  );
};

Tags.defaultProps = {
  currentNote: '',
};

Tags.propTypes = {
  inTags: PropTypes.array.isRequired,
  updateNote: PropTypes.func.isRequired,
  saveNote: PropTypes.func.isRequired,
  currentNote: PropTypes.string,
};

export default Tags;
