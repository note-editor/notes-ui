import React from 'react';
import PropTypes from 'prop-types';

const TitleInput = ({
  title,
  updateTitle,
  currentNote,
  saveNote,
}) => (
  <input
    type="text"
    className="input"
    placeholder="Enter Title Here"
    value={title}
    onChange={(e) => { updateTitle(currentNote, e.target.value); }}
    onBlur={() => { saveNote(currentNote); }}
  />
);

TitleInput.defaultProps = {
  currentNote: '',
};

TitleInput.propTypes = {
  currentNote: PropTypes.string,
  title: PropTypes.string.isRequired,
  updateTitle: PropTypes.func.isRequired,
  saveNote: PropTypes.func.isRequired,
};

export default TitleInput;
