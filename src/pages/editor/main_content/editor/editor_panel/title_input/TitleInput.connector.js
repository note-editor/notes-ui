import { connect } from 'react-redux';
import { updateNote, saveNote } from '../../../../../../redux/action_creators/Notes.actioncreator';
import TitleInput from './TitleInput.component';

const mapStateToProps = state => ({
  title: state.Notes.data[state.Notes.currentNote].title,
  currentNote: state.Notes.currentNote,
});

const mapDispatchToProps = dispatch => ({
  updateTitle: (currentNote, title) => dispatch(updateNote(currentNote, 'title', title)),
  saveNote: currentNote => dispatch(saveNote(currentNote)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(TitleInput);
