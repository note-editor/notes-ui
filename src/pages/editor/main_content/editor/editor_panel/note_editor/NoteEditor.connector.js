import { connect } from 'react-redux';
import { updateNote, saveNote } from '../../../../../../redux/action_creators/Notes.actioncreator';
import NoteEditor from './NoteEditor.component';


const mapStateToProps = state => ({
  currentNote: state.Notes.currentNote,
  noteContent: state.Notes.data[state.Notes.currentNote].content,
});

const mapDispatchToProps = dispatch => ({
  updateContent: (currentNote, content) => dispatch(updateNote(currentNote, 'content', content)),
  saveNote: currentNote => dispatch(saveNote(currentNote)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(NoteEditor);
