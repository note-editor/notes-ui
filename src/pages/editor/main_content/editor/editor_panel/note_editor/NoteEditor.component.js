import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ReactQuill from 'react-quill';
import { connect } from 'react-redux';

class NoteEditor extends Component {
  constructor(props) {
    super(props);
    const { noteContent } = this.props;
    this.state = { text: noteContent, shouldBlockReload: false };
    this.handleChange = this.handleChange.bind(this);
    this.handleLeavePage = this.handleLeavePage.bind(this);
  }

  componentDidMount() {
    window.addEventListener('beforeunload', this.handleLeavePage);
  }

  componentWillUnmount() {
    window.removeEventListener('beforeunload', this.handleLeavePage);
  }

  handleChange(value) {
    this.setState({ text: value, shouldBlockReload: true });
    const { text } = this.state;
    const { updateContent, currentNote } = this.props;
    updateContent(currentNote, text);
  }

  handleLeavePage(e) {
    const { saveNote, currentNote } = this.props;
    const { shouldBlockReload } = this.state;

    if (shouldBlockReload === true) {
      e.returnValue = '';
      saveNote(currentNote);
      this.setState({ shouldBlockReload: false });
      return '';
    }

    return null;
  }

  render() {
    const { text } = this.state;
    const { saveNote, currentNote } = this.props;

    return (
      <ReactQuill
        value={text || ''}
        key="quill"
        onChange={this.handleChange}
        onBlur={() => {
          saveNote(currentNote);
          this.setState({ shouldBlockReload: false });
        }}
      />
    );
  }
}

NoteEditor.defaultProps = {
  currentNote: '',
};

NoteEditor.propTypes = {
  currentNote: PropTypes.string,
  noteContent: PropTypes.string.isRequired,
  updateContent: PropTypes.func.isRequired,
  saveNote: PropTypes.func.isRequired,
};

export default connect()(NoteEditor);
