import { connect } from 'react-redux';
import EditorPanel from './EditorPanel.component';

const mapStateToProps = state => ({
  currentNote: state.Notes.currentNote,
});

const mapDispatchToProps = () => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(EditorPanel);
