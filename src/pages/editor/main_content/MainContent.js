import React from 'react';

import SideBar from './sidebar/Sidebar.component';
import EditorPanelContainer from './editor/EditorPanel.connector';

const MainContent = () => (
  <div id="main-content-container">
    <SideBar />
    <EditorPanelContainer />
  </div>
);

export default MainContent;
