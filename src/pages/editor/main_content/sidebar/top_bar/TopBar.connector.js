import { connect } from 'react-redux';
import { addNote } from '../../../../../redux/action_creators/Notes.actioncreator';
import { addFolder } from '../../../../../redux/action_creators/Folders.actioncreator';
import { setFilterText, setMenuDisplay } from '../../../../../redux/action_creators/Ui.actioncreator';
import TopBar from './TopBar.component';

const mapStateToProps = state => ({
  filter: state.Ui.filterText,
  currentLocation: state.Folders.currentFolder,
});

function mapDispatchToProps(dispatch) {
  return {
    addNote: parentId => dispatch(addNote(parentId)),
    addFolder: parentId => dispatch(addFolder(parentId)),
    setFilterText: filterText => dispatch(setFilterText(filterText)),
    showMenu: () => dispatch(setMenuDisplay(true)),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(TopBar);
