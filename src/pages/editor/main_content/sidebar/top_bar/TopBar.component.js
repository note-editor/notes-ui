import React from 'react';
import PropTypes from 'prop-types';
import {
  faPlus,
  faFolderPlus,
  faBars,
} from '@fortawesome/free-solid-svg-icons';
import ButtonIcon from '../../../../../assets/components/button_icon/ButtonIcon.component';
import Search from './search/Search.connector';

const TopBar = (props) => {
  const {
    addNote,
    addFolder,
    currentLocation,
    showMenu,
  } = props;

  return (
    <div
      id="top-bar"
      className="top-bar-height"
    >
      <div className="add-content">
        <ButtonIcon
          icon={faBars}
          funct={showMenu}
        />
        <ButtonIcon
          icon={faFolderPlus}
          funct={addFolder}
          args={[currentLocation]}
        />
        <ButtonIcon
          icon={faPlus}
          funct={addNote}
          args={[currentLocation]}
        />
      </div>
      <Search />
    </div>
  );
};

TopBar.defaultProps = {
  currentLocation: null,
};

TopBar.propTypes = {
  addNote: PropTypes.func.isRequired,
  addFolder: PropTypes.func.isRequired,
  showMenu: PropTypes.func.isRequired,
  currentLocation: PropTypes.string,
};

export default TopBar;
