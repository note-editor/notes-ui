import React from 'react';
import PropTypes from 'prop-types';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch } from '@fortawesome/free-solid-svg-icons';

const Search = ({ filter, setFilterText }) => (
  <div className="search">
    <div className="is-full">
      <p className="control has-icons-left">
        <input
          type="text"
          className="input is-small"
          value={filter}
          onChange={(e) => { setFilterText(e.target.value); }}
        />
        <span className="icon is-small is-left">
          <FontAwesomeIcon icon={faSearch} />
        </span>
      </p>
    </div>
  </div>
);

Search.defaultProps = {
  filter: '',
};

Search.propTypes = {
  setFilterText: PropTypes.func.isRequired,
  filter: PropTypes.string,
};

export default Search;
