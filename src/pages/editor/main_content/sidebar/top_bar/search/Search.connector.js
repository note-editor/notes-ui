import { connect } from 'react-redux';
import { setFilterText } from '../../../../../../redux/action_creators/Ui.actioncreator';
import Search from './Search.component';

const mapStateToProps = state => ({
  filter: state.Ui.filterText,
  currentLocation: state.Folders.currentFolder,
});

const mapDispatchToProps = dispatch => ({
  setFilterText: filterText => dispatch(setFilterText(filterText)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Search);
