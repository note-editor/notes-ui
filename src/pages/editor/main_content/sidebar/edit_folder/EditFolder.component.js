import React from 'react';
import PropTypes from 'prop-types';

// Wrapper object for the note
const EditFolder = (props) => {
  const {
    display,
    folder,
    setFolderEdit,
    updateTitle,
    saveFolder,
  } = props;

  if (display === true) {
    return (
      <div className="modal is-active">
        <div
          className="modal-background"
          onMouseDown={() => {
            setFolderEdit('', false);
            saveFolder(folder.id);
          }}
          role="button"
          tabIndex={0}
        />
        <div
          className="modal-content"
          onMouseDown={(e) => {
            e.stopPropagation();
          }}
          role="button"
          tabIndex={0}
        >
          <div className="box has-text-centered">
            <h5 className="title is-5">
              Edit Folder Details
            </h5>
            <div className="columns">
              <div className="column" />
              <div className="column is-two">
                Folder Title:
              </div>
              <div className="column is-two">
                <input
                  type="text"
                  className="input"
                  placeholder="Enter Title Here"
                  value={folder.title}
                  onChange={(e) => { updateTitle(folder.id, e.target.value); }}
                  onBlur={() => { saveFolder(folder.id); }}
                />
              </div>
              <div className="column" />
            </div>
            <div className="columns">
              <div className="column" />
              <div className="column is-two" />
              <div className="column is-two align-content-right">
                <input
                  type="button"
                  className="button"
                  key="btn2"
                  onMouseDown={() => {
                    setFolderEdit('', false);
                    saveFolder(folder.id);
                  }}
                  value="Save"
                />
              </div>
              <div className="column" />
            </div>
          </div>
        </div>
        <button
          type="button"
          className="modal-close is-large"
          onMouseDown={() => {
            setFolderEdit('', false);
            saveFolder(folder.id);
          }}
          aria-label="close"
        />
      </div>
    );
  }

  return null;
};

EditFolder.defaultProps = {
  folder: {},
};

EditFolder.propTypes = {
  display: PropTypes.bool.isRequired,
  folder: PropTypes.object,
  setFolderEdit: PropTypes.func.isRequired,
  updateTitle: PropTypes.func.isRequired,
  saveFolder: PropTypes.func.isRequired,
};

export default EditFolder;
