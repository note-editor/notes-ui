import { connect } from 'react-redux';
import { setFolderEdit } from '../../../../../redux/action_creators/Ui.actioncreator';
import { updateFolder, saveFolder } from '../../../../../redux/action_creators/Folders.actioncreator';

import EditFolder from './EditFolder.component';

const mapStateToProps = state => ({
  display: state.Ui.folderEdit.display,
  folder: state.Folders.data[state.Ui.folderEdit.id],
});

const mapDispatchToProps = dispatch => ({
  setFolderEdit: (id, display) => dispatch(setFolderEdit(id, display)),
  updateTitle: (currentFolder, title) => dispatch(updateFolder(currentFolder, 'title', title)),
  saveFolder: currentFolder => dispatch(saveFolder(currentFolder)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(EditFolder);
