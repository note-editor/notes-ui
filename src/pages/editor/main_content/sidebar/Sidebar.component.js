import React from 'react';

import TopBar from './top_bar/TopBar.connector';
import DeleteConfirmationMessage from './delete_confirmation/DeleteConfirmationMessage.connector';
import EditFolder from './edit_folder/EditFolder.connector';
import NoteSection from './sections/note_section/NoteSection.connector';
import FolderSection from './sections/folder_section/FolderSection.connector';

const Sidebar = () => (
  <div
    id="sidebar"
    className="sidebar-width"
  >
    <TopBar />
    <DeleteConfirmationMessage />
    <EditFolder />
    <div id="items-container">
      <FolderSection />
      <NoteSection />
    </div>
  </div>
);

export default Sidebar;
