import React from 'react';
import PropTypes from 'prop-types';

const DeleteContent = (props) => {
  const {
    toggleDeleteMessage,
    data,
    deleteFunction,
  } = props;

  return (
    <div className="box has-text-centered">
      Are you sure you want to delete:&nbsp;
      <br />
      {data.title}
      ?
      <br />
      <div className="columns">
        <div className="column" />
        <div className="column is-two">
          <input
            type="button"
            className="button"
            key="btn1"
            onMouseDown={(e) => {
              e.stopPropagation();
              deleteFunction(data.id);
            }}
            value="Yes"
          />
        </div>
        <br />
        <div className="column is-two">
          <input
            type="button"
            className="button"
            key="btn2"
            onMouseDown={toggleDeleteMessage}
            value="No"
          />
        </div>
        <div className="column" />
      </div>
    </div>
  );
};

DeleteContent.propTypes = {
  deleteFunction: PropTypes.func.isRequired,
  toggleDeleteMessage: PropTypes.func.isRequired,
  data: PropTypes.object.isRequired,
};

const DeleteConfirmationMessage = (props) => {
  const {
    display,
    toggleDeleteMessage,
    note,
    folder,
    deleteNote,
    deleteFolder,
  } = props;

  let deleteContent;

  if (display) {
    if (Object.keys(note).length !== 0) {
      deleteContent = (
        <DeleteContent
          toggleDeleteMessage={toggleDeleteMessage}
          data={note}
          deleteFunction={deleteNote}
        />
      );
    }

    if (Object.keys(folder).length !== 0) {
      deleteContent = (
        <DeleteContent
          toggleDeleteMessage={toggleDeleteMessage}
          data={folder}
          deleteFunction={deleteFolder}
        />
      );
    }

    return (
      <div className="modal is-active">
        <div
          className="modal-background"
          onMouseDown={toggleDeleteMessage}
          role="button"
          tabIndex={0}
        />
        <div
          className="modal-content"
          onMouseDown={(e) => {
            e.stopPropagation();
          }}
          role="button"
          tabIndex={0}
        >
          {deleteContent}
        </div>
        <button
          type="button"
          className="modal-close is-large"
          onMouseDown={toggleDeleteMessage}
          aria-label="close"
        />
      </div>
    );
  }
  return (null);
};

DeleteConfirmationMessage.defaultProps = {
  note: {},
  folder: {},
};

DeleteConfirmationMessage.propTypes = {
  deleteNote: PropTypes.func.isRequired,
  deleteFolder: PropTypes.func.isRequired,
  toggleDeleteMessage: PropTypes.func.isRequired,
  display: PropTypes.bool.isRequired,
  note: PropTypes.object,
  folder: PropTypes.object,
};

export default DeleteConfirmationMessage;
