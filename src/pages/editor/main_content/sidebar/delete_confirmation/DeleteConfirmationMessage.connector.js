import { connect } from 'react-redux';
import { toggleDeleteMessage, deleteNote, deleteFolder } from '../../../../../redux/action_creators/Delete.actioncreator';
import DeleteNoteMessage from './DeleteConfirmationMessage.component';

const mapStateToProps = state => ({
  display: state.Delete.display,
  note: state.Notes.data[state.Delete.noteId],
  folder: state.Folders.data[state.Delete.folderId],
});

const mapDispatchToProps = dispatch => ({
  deleteNote: noteId => dispatch(deleteNote(noteId)),
  deleteFolder: folderId => dispatch(deleteFolder(folderId)),
  toggleDeleteMessage: () => dispatch(toggleDeleteMessage()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(DeleteNoteMessage);
