import { connect } from 'react-redux';
import { setDraggedItem } from '../../../../../../redux/action_creators/Ui.actioncreator';
import Draggable from './Draggable.component';

const mapStateToProps = () => ({});

const mapDispatchToProps = dispatch => ({
  setDraggedItem: (id, type) => dispatch(setDraggedItem(id, type)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Draggable);
