import React from 'react';
import PropTypes from 'prop-types';

// The note selector object
const Draggable = ({
  itemId,
  children,
  setDraggedItem,
  itemType,
}) => (
  <div
    draggable
    onDragStart={() => { setDraggedItem(itemId, itemType); }}
  >
    {children}
  </div>
);

Draggable.propTypes = {
  itemId: PropTypes.string.isRequired,
  setDraggedItem: PropTypes.func.isRequired,
  children: PropTypes.object.isRequired,
  itemType: PropTypes.string.isRequired,
};

export default Draggable;
