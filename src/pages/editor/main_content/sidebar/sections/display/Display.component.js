import React from 'react';
import PropTypes from 'prop-types';
import filter from '../../../../../../assets/utilities/filter';

// Wrapper object for the note
const Display = ({
  children,
  item,
  searchText,
  currentLocation,
}) => {
  let display = true;

  // If the note selector should be displayed base on the user search
  if (searchText !== '') {
    display = filter(item, searchText);
  } else if (item.parent !== currentLocation) {
    display = false;
  }

  return display ? (
    <div>
      {children}
    </div>
  ) : null;
};

Display.defaultProps = {
  currentLocation: null,
};

Display.propTypes = {
  children: PropTypes.object.isRequired,
  item: PropTypes.object.isRequired,
  searchText: PropTypes.string.isRequired,
  currentLocation: PropTypes.string,
};

export default Display;
