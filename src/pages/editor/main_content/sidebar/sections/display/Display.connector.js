import { connect } from 'react-redux';
import Display from './Display.component';

const mapStateToProps = state => ({
  searchText: state.Ui.filterText,
  currentLocation: state.Folders.currentFolder,
});

const mapDispatchToProps = () => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Display);
