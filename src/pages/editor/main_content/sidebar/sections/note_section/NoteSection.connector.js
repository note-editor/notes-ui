import { connect } from 'react-redux';
import { setCurrentNote } from '../../../../../../redux/action_creators/Notes.actioncreator';
import NoteSection from './NoteSection.component';

const mapStateToProps = state => ({
  notes: state.Notes.data,
});

const mapDispatchToProps = dispatch => ({
  setCurrentNote: note => dispatch(setCurrentNote(note)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(NoteSection);
