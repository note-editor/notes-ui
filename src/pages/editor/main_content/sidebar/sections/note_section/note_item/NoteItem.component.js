import React from 'react';
import PropTypes from 'prop-types';

import NoteSelectorContainer from '../note_selector/NoteSelector.connector';
import Display from '../../display/Display.connector';
import SelectorWrapper from '../../selector_wrapper/SelectorWrapper.connector';
import Draggable from '../../draggable/Draggable.connector';

const NoteItem = ({ note, setCurrentNote }) => (
  <Display
    item={note}
    key={note.id}
  >
    <SelectorWrapper
      item={note}
      key={note.id}
      setCurrentItem={setCurrentNote}
    >
      <Draggable
        itemId={note.id}
        itemType="note"
      >
        <NoteSelectorContainer
          note={note}
          key={note.id}
        />
      </Draggable>
    </SelectorWrapper>
  </Display>
);

NoteItem.propTypes = {
  note: PropTypes.object.isRequired,
  setCurrentNote: PropTypes.func.isRequired,
};

export default NoteItem;
