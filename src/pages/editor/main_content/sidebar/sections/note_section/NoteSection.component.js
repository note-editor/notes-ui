import React from 'react';
import PropTypes from 'prop-types';

import NoteItem from './note_item/NoteItem.component';

const NoteSection = ({
  notes,
  setCurrentNote,
}) => (
  <div id="select-note-container">
    {Object.keys(notes).map(noteId => (
      <NoteItem
        key={noteId}
        setCurrentNote={setCurrentNote}
        note={notes[noteId]}
      />
    ))}
  </div>
);

NoteSection.defaultProps = {
  notes: {},
};

NoteSection.propTypes = {
  notes: PropTypes.object,
  setCurrentNote: PropTypes.func.isRequired,
};

export default NoteSection;
