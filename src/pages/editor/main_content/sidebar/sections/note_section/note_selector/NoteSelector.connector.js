import { connect } from 'react-redux';
import { setCurrentNote } from '../../../../../../../redux/action_creators/Notes.actioncreator';
import { setNoteToDelete } from '../../../../../../../redux/action_creators/Delete.actioncreator';
import NoteSelector from './NoteSelector.component';

const mapStateToProps = state => ({
  currentLocation: state.Folders.currentFolder,
  currentNote: state.Notes.currentNote,
});

const mapDispatchToProps = dispatch => ({
  setCurrentNote: note => dispatch(setCurrentNote(note)),
  setNoteToDelete: note => dispatch(setNoteToDelete(note)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(NoteSelector);
