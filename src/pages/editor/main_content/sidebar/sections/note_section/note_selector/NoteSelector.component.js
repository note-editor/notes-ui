import React from 'react';
import PropTypes from 'prop-types';

import { faBook, faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { convertTagsToText } from '../../../../../../../assets/utilities/tags';


// Displays the note header and the note delete button
const Header = ({ titleText, setNoteToDelete, noteId }) => (
  <header className="card-header">
    <span
      className="card-header-icon"
    >
      <FontAwesomeIcon icon={faBook} />
    </span>
    <span className="card-header-title" title={titleText}>
      {titleText}
    </span>
    <span
      className="card-header-icon"
      onMouseDown={(e) => {
        e.stopPropagation();
        setNoteToDelete(noteId);
      }}
      role="button"
      tabIndex={0}
    >
      <FontAwesomeIcon icon={faTrashAlt} />
    </span>
  </header>
);

Header.defaultProps = {
  titleText: 'Title',
};

Header.propTypes = {
  titleText: PropTypes.string,
  setNoteToDelete: PropTypes.func.isRequired,
  noteId: PropTypes.string.isRequired,
};

// Displays the notes tags
const Content = ({ tagsText }) => {
  if (tagsText !== '') {
    return (
      <div className="card-content">
        <div className="content tags">
          {tagsText}
        </div>
      </div>
    );
  }
  return null;
};

Content.propTypes = {
  tagsText: PropTypes.string.isRequired,
};

// The note selector object
const NoteSelector = (props) => {
  const {
    note,
    setNoteToDelete,
  } = props;

  const tagsText = convertTagsToText(note.tags);

  return (
    <div>
      <Header
        titleText={note.title}
        setNoteToDelete={setNoteToDelete}
        noteId={note.id}
      />
      <Content
        tagsText={tagsText}
      />
    </div>

  );
};

NoteSelector.propTypes = {
  note: PropTypes.object.isRequired,
  setNoteToDelete: PropTypes.func.isRequired,
};

export default NoteSelector;
