import React from 'react';
import PropTypes from 'prop-types';

import { faTrashAlt, faFolder, faPencilAlt } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const FolderSelector = (props) => {
  const {
    folder,
    setFolderToDelete,
    setFolderEdit,
  } = props;

  return (
    <div>
      <header className="card-header">
        <span className="card-header-icon">
          <FontAwesomeIcon icon={faFolder} />
        </span>
        <p className="card-header-title">
          {folder.title}
        </p>
        <span
          className="card-header-icon"
          onMouseDown={(e) => {
            e.stopPropagation();
            setFolderEdit(folder.id, true);
          }}
          role="button"
          tabIndex={0}
        >
          <FontAwesomeIcon icon={faPencilAlt} />
        </span>
        <span
          className="card-header-icon"
          onMouseDown={(e) => {
            e.stopPropagation();
            setFolderToDelete(folder.id);
          }}
          role="button"
          tabIndex={0}
        >
          <FontAwesomeIcon icon={faTrashAlt} />
        </span>
      </header>
    </div>

  );
};

FolderSelector.propTypes = {
  folder: PropTypes.object.isRequired,
  setFolderToDelete: PropTypes.func.isRequired,
  setFolderEdit: PropTypes.func.isRequired,
};

export default FolderSelector;
