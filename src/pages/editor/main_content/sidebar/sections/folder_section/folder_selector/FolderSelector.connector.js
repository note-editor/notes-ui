import { connect } from 'react-redux';
import { setFolderToDelete } from '../../../../../../../redux/action_creators/Delete.actioncreator';
import { updateNote } from '../../../../../../../redux/action_creators/Notes.actioncreator';
import { setFolderEdit } from '../../../../../../../redux/action_creators/Ui.actioncreator';
import FolderSelector from './FolderSelector.component';

const mapStateToProps = state => ({
  currentLocation: state.Folders.currentFolder,
  draggedNote: state.Notes.draggedNote,
});

const mapDispatchToProps = dispatch => ({
  setFolderToDelete: folderId => dispatch(setFolderToDelete(folderId)),
  updateNote: (noteId, key, value) => dispatch(updateNote(noteId, key, value)),
  setFolderEdit: (id, display) => dispatch(setFolderEdit(id, display)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(FolderSelector);
