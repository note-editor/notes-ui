import React from 'react';
import PropTypes from 'prop-types';

import FolderSelector from '../folder_selector/FolderSelector.connector';
import Droppable from '../../droppable/Droppable.connector';
import SelectorWrapper from '../../selector_wrapper/SelectorWrapper.connector';

const PreviousFolder = ({ folder, setCurrentFolder }) => (
  <SelectorWrapper
    item={folder.id}
    key={folder.id}
    setCurrentItem={setCurrentFolder}
    itemType="folder"
  >
    <Droppable
      key={folder.id}
      folder={folder}
    >
      <FolderSelector
        key={folder.id}
        folder={folder}
      />
    </Droppable>
  </SelectorWrapper>
);

PreviousFolder.propTypes = {
  folder: PropTypes.object.isRequired,
  setCurrentFolder: PropTypes.func.isRequired,
};

export default PreviousFolder;
