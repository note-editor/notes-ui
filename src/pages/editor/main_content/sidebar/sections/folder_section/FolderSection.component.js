import React from 'react';
import PropTypes from 'prop-types';

import PreviousFolder from './previous_folder/PreviousFolder.component';
import FolderItem from './folder_item/FolderItem.component';

const FolderSection = ({ folders, setCurrentFolder, currentFolder }) => (
  <div>
    {currentFolder
      ? (
        <PreviousFolder
          folder={folders[currentFolder]}
          setCurrentFolder={setCurrentFolder}
        />
      ) : null}
    {Object.keys(folders).map(folderId => (
      <FolderItem
        key={folderId}
        folder={folders[folderId]}
        setCurrentFolder={setCurrentFolder}
      />
    ))}
  </div>
);

FolderSection.defaultProps = {
  currentFolder: null,
};

FolderSection.propTypes = {
  folders: PropTypes.object.isRequired,
  setCurrentFolder: PropTypes.func.isRequired,
  currentFolder: PropTypes.string,
};

export default FolderSection;
