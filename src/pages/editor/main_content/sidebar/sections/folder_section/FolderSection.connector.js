import { connect } from 'react-redux';
import { setCurrentFolder } from '../../../../../../redux/action_creators/Folders.actioncreator';

import FolderSection from './FolderSection.component';

const mapStateToProps = state => ({
  folders: state.Folders.data,
  currentFolder: state.Folders.currentFolder,
});

const mapDispatchToProps = dispatch => ({
  setCurrentFolder: folder => dispatch(setCurrentFolder(folder)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(FolderSection);
