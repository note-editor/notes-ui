import React from 'react';
import PropTypes from 'prop-types';

import FolderSelector from '../folder_selector/FolderSelector.connector';
import SelectorWrapper from '../../selector_wrapper/SelectorWrapper.connector';
import Display from '../../display/Display.connector';
import Droppable from '../../droppable/Droppable.connector';
import Draggable from '../../draggable/Draggable.connector';

const FolderItem = ({ folder, setCurrentFolder }) => (
  <Display
    item={folder}
    key={folder.id}
  >
    <SelectorWrapper
      item={folder}
      key={folder.id}
      setCurrentItem={setCurrentFolder}
    >
      <Droppable
        key={folder.id}
        folder={folder}
      >
        <Draggable
          itemId={folder.id}
          itemType="folder"
        >
          <FolderSelector
            key={folder.id}
            folder={folder}
          />
        </Draggable>
      </Droppable>
    </SelectorWrapper>
  </Display>
);

FolderItem.propTypes = {
  folder: PropTypes.object.isRequired,
  setCurrentFolder: PropTypes.func.isRequired,
};

export default FolderItem;
