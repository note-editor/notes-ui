import { connect } from 'react-redux';
import { updateNote, saveNote } from '../../../../../../redux/action_creators/Notes.actioncreator';
import { updateFolder, saveFolder } from '../../../../../../redux/action_creators/Folders.actioncreator';
import Droppable from './Droppable.component';

const mapStateToProps = state => ({
  currentLocation: state.Folders.currentFolder,
  draggedItem: state.Ui.draggedItem,
});

const mapDispatchToProps = dispatch => ({
  updateItem: (draggedItem, key, value) => {
    if (draggedItem.type === 'note') {
      dispatch(updateNote(draggedItem.id, key, value));
      dispatch(saveNote(draggedItem.id));
    } else {
      dispatch(updateFolder(draggedItem.id, key, value));
      dispatch(saveFolder(draggedItem.id));
    }
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Droppable);
