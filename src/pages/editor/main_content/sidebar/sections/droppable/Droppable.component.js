import React from 'react';
import PropTypes from 'prop-types';

// The note selector object
const Droppable = (props) => {
  const {
    currentLocation,
    children,
    updateItem,
    draggedItem,
    folder,
  } = props;

  return (
    <div
      onDragOver={(e) => { e.preventDefault(); }}
      onDrop={() => {
        if (draggedItem.id !== folder.id) {
          if (currentLocation === folder.id) {
            updateItem(draggedItem, 'parent', folder.parent);
          } else {
            updateItem(draggedItem, 'parent', folder.id);
          }
        }
      }}
    >
      {children}
    </div>
  );
};

Droppable.defaultProps = {
  currentLocation: '',
};

Droppable.propTypes = {
  currentLocation: PropTypes.string,
  updateItem: PropTypes.func.isRequired,
  draggedItem: PropTypes.object.isRequired,
  folder: PropTypes.object.isRequired,
  children: PropTypes.object.isRequired,
};

export default Droppable;
