import React from 'react';
import PropTypes from 'prop-types';

// Wrapper object for the note
const SelectorWrapper = (props) => {
  const {
    children,
    item,
    currentLocation,
    setCurrentItem,
    currentNote,
    saveNote,
    toSave,
  } = props;

  let classes = '';

  if (currentNote === item.id || item.id === currentLocation) {
    classes = 'card selected';
  } else {
    classes = 'card';
  }

  return (
    <div
      className={classes}
      onMouseUp={() => {
        if (item.id === currentLocation) {
          setCurrentItem(item.parent);
        } else {
          setCurrentItem(item.id);
          if (currentNote !== '' && toSave === true) {
            saveNote(currentNote);
          }
        }
      }}
      role="button"
      tabIndex={0}
    >
      {children}
    </div>
  );
};

SelectorWrapper.defaultProps = {
  currentLocation: null,
  currentNote: '',
};

SelectorWrapper.propTypes = {
  children: PropTypes.object.isRequired,
  item: PropTypes.object.isRequired,
  setCurrentItem: PropTypes.func.isRequired,
  currentLocation: PropTypes.string,
  currentNote: PropTypes.string,
  saveNote: PropTypes.func.isRequired,
  toSave: PropTypes.bool.isRequired,
};

export default SelectorWrapper;
