import { connect } from 'react-redux';
import { saveNote } from '../../../../../../redux/action_creators/Notes.actioncreator';
import SelectorWrapper from './SelectorWrapper.component';

const mapStateToProps = state => ({
  searchText: state.Ui.filterText,
  currentLocation: state.Folders.currentFolder,
  currentNote: state.Notes.currentNote,
  toSave: state.Notes.toSave,
});

const mapDispatchToProps = dispatch => ({
  saveNote: currentNote => dispatch(saveNote(currentNote)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SelectorWrapper);
