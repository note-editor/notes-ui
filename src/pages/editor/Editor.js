import React from 'react';

import MainContent from './main_content/MainContent';
import MenuContainer from './menu_container/MenuContainer.conector';

const Editor = () => (
  <section className="body-section">
    <div
      id="app-container"
    >
      <link rel="stylesheet" href="//cdn.quilljs.com/1.2.6/quill.snow.css" />
      <MenuContainer />
      <MainContent />
    </div>
  </section>
);

export default Editor;
