import { connect } from 'react-redux';
import Menu from './Menu.component';
import { setMenuDisplay } from '../../../../redux/action_creators/Ui.actioncreator';
import { clearBearerToken } from '../../../../redux/action_creators/Auth.actioncreator';

const mapStateToProps = state => ({
  display: state.Ui.menuDisplay,
});

const mapDispatchToProps = dispatch => ({
  close: () => dispatch(setMenuDisplay(false)),
  logout: () => dispatch(clearBearerToken()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Menu);
