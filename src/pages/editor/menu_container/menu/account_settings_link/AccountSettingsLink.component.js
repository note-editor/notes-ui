import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUser } from '@fortawesome/free-solid-svg-icons';

const AccountSettingsLink = () => (
  <div className="menu-item">
    <span className="profile-icon-container">
      <FontAwesomeIcon icon={faUser} />
    </span>
    <span className="profile-icon-text">
      Account Settings
    </span>
    <hr />
  </div>
);

export default AccountSettingsLink;
