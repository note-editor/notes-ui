import React from 'react';
import PropTypes from 'prop-types';

const MenuLink = ({ text, funct, args }) => (
  <div className="menu-item">
    <span
      className="text menu-link"
      onMouseDown={
        () => {
          funct(...args);
        }
      }
      role="button"
      tabIndex={0}
    >
      {text}
    </span>
  </div>
);

MenuLink.defaultProps = {
  args: [],
};

MenuLink.propTypes = {
  funct: PropTypes.func.isRequired,
  text: PropTypes.string.isRequired,
  args: PropTypes.array,
};

export default MenuLink;
