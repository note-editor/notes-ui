import React from 'react';
import PropTypes from 'prop-types';

import { faTimesCircle } from '@fortawesome/free-solid-svg-icons';
import AccountSettingsLink from './account_settings_link/AccountSettingsLink.component';
import MenuLink from './menu_link/MenuLink.component';
import ButtonIcon from '../../../../assets/components/button_icon/ButtonIcon.component';

const Menu = ({ close, logout }) => (
  <div className="menu-wrapper sidebar-width">
    <ButtonIcon
      icon={faTimesCircle}
      funct={close}
      className="close"
    />
    <div className="menu-container">
      <AccountSettingsLink />
      <MenuLink
        text="Logout"
        funct={logout}
      />
    </div>
  </div>
);

Menu.propTypes = {
  close: PropTypes.func.isRequired,
  logout: PropTypes.func.isRequired,
};

export default Menu;
