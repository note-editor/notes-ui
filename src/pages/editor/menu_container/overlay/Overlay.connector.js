import { connect } from 'react-redux';
import Overlay from './Overlay.component';
import { setMenuDisplay } from '../../../../redux/action_creators/Ui.actioncreator';

const mapStateToProps = () => ({});

const mapDispatchToProps = dispatch => ({
  close: () => dispatch(setMenuDisplay(false)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Overlay);
