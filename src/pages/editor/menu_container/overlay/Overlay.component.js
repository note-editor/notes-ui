import React from 'react';
import PropTypes from 'prop-types';

const Overlay = ({ close }) => (
  <div
    id="menu-overlay"
    onMouseDown={() => {
      close();
    }}
    role="button"
    tabIndex={0}
  />
);

Overlay.propTypes = {
  close: PropTypes.func.isRequired,
};

export default Overlay;
