import React from 'react';
import PropTypes from 'prop-types';

import Menu from './menu/Menu.connector';
import MenuOverlay from './overlay/Overlay.connector';

const MenuContainer = ({ display }) => (
  display ? (
    <div id="menu-container">
      <Menu />
      <MenuOverlay />
    </div>
  ) : null
);

MenuContainer.propTypes = {
  display: PropTypes.bool.isRequired,
};

export default MenuContainer;
