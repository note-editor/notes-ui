import { connect } from 'react-redux';
import MenuContainer from './MenuContainer.component';

const mapStateToProps = state => ({
  display: state.Ui.menuDisplay,
});

const mapDispatchToProps = () => ({
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(MenuContainer);
