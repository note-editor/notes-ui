import React from 'react';
import PropTypes from 'prop-types';
import './assets/styles/styles.min.css';
import { getCookie } from './assets/utilities/cookies';

import Layout from './assets/components/layout/Layout.connector';
import Editor from './pages/editor/Editor';

const App = ({ load, offlineMode }) => {
  const authData = getCookie('authData');

  if (authData || offlineMode) {
    if (!offlineMode) {
      load(authData);
    }

    return (
      <Layout>
        <Editor />
      </Layout>
    );
  }

  // Redirect to authentication address.
  window.location.href = `${process.env.REACT_APP_AUTH_URL}?returnAdd=${window.location.href}`;
  return null;
};

App.propTypes = {
  load: PropTypes.func.isRequired,
  offlineMode: PropTypes.bool.isRequired,
};

export default App;
