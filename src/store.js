import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import multi from 'redux-multi';
import createRootReducer from './redux/reducers/RootReducer';
import history from './assets/utilities/history';

const middlewareCompose = compose(
  applyMiddleware(
    multi,
    thunk,
  ),
);

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(
  createRootReducer(history),
  composeEnhancers(middlewareCompose),
);

export default store;
